import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import com.mysql.jdbc.PreparedStatement;



public class Interpreter {
	public static ArrayList<Question> getQuestions(int level) throws SQLException{//gets all questions at a particular level

		ArrayList<Question> questions=new ArrayList<Question>();
		com.mysql.jdbc.PreparedStatement getQuestion = (PreparedStatement) Connection.con.prepareStatement("SELECT * FROM `questions` where level=? GROUP by question_id");
		getQuestion.setInt(1, level);
		ResultSet rs=getQuestion.executeQuery();
		System.out.println("\nChoose something to say:");
		while(rs.next()){
			Question question=new Question();
			question.setStatement(rs.getString("Question"));
			question.setId(rs.getInt("id"));
			System.out.println(rs.getInt("id")+":"+rs.getString("Question"));
			questions.add(question);
		}
		return questions;
	}
	public static ArrayList<Question> getAnswers(int  level) throws SQLException{

		ArrayList<Question> questions=new ArrayList<Question>();
		com.mysql.jdbc.PreparedStatement getQuestion = (PreparedStatement) Connection.con.prepareStatement("SELECT * FROM `questions` where level=? GROUP by question_id");
		getQuestion.setInt(1, level);
		ResultSet rs=getQuestion.executeQuery();
		System.out.println("\nChoose something to say:");
		while(rs.next()){
			Question question=new Question();
			question.setStatement(rs.getString("Question"));
			question.setId(rs.getInt("id"));
			System.out.println(rs.getInt("id")+":"+rs.getString("Question"));
			questions.add(question);
		}
		return questions;
	}
	public static ArrayList<Question> getCounterQuestions(int counter_question,int level) throws SQLException{

		ArrayList<Question> questions=new ArrayList<Question>();
		com.mysql.jdbc.PreparedStatement getQuestion = (PreparedStatement) Connection.con.prepareStatement("SELECT * FROM `questions` where level=? and question_id=? GROUP by question_id");
		getQuestion.setInt(1, level);
		getQuestion.setInt(2, counter_question);
		ResultSet rs=getQuestion.executeQuery();
		System.out.println("\nChoose something to say:");
		while(rs.next()){
			Question question=new Question();
			question.setStatement(rs.getString("Question"));
			question.setId(rs.getInt("id"));
			System.out.println(rs.getInt("id")+":"+rs.getString("Question"));
			questions.add(question);
		}
		return questions;
	}
	public static Answer getAnswer(int question) throws SQLException{

		Answer answer=new Answer();
		com.mysql.jdbc.PreparedStatement getAnswer = (PreparedStatement) Connection.con.prepareStatement("SELECT * FROM `answers` where id=(Select answer from `questions` where id=?)");

		getAnswer.setInt(1, question);
		ResultSet rs=getAnswer.executeQuery();
		if(!rs.next()){System.out.println("Oops I cant answer that");return null;}
		answer.setAnswer_statement(rs.getString("Answer"));
		System.out.println("RI:"+rs.getString("Answer"));
		answer.setId(rs.getInt("id"));
		answer.setCounter_question(rs.getInt("Counter_question"));
		return answer;
	}
	public static Question getQuestion(int question_id) throws SQLException{


		com.mysql.jdbc.PreparedStatement getQuestion = (PreparedStatement) Connection.con.prepareStatement("SELECT * FROM `questions` where question_id=? GROUP by question_id");
		getQuestion.setInt(1, question_id);
		ResultSet rs=getQuestion.executeQuery();
		if(!rs.next()){System.out.println("I dont know what to say");return null;}

		Question question=new Question();
		question.setStatement(rs.getString("Question"));
		question.setId(rs.getInt("id"));
		question.setAnswer(rs.getInt("Answer"));
		System.out.println(rs.getInt("id")+":"+rs.getString("Question"));



		return question;
	}
	public static void main(String[] args) throws SQLException {
		new Connection();
		boolean exit=false;

		Scanner in=new Scanner(System.in);
		int input;
		int level=1;
		while(!exit){
			level=1;
			ArrayList<Question> questions = getQuestions(level);

			input=in.nextInt();
			Answer answer = getAnswer(input);

			if(answer.getCounter_question()==-1){System.exit(0);}
			if(answer.getCounter_question()!=0){
				level++;
				getCounterQuestions(answer.getCounter_question(), level);
				System.out.println("0:OK");
				input=in.nextInt();

				if(input==0){System.out.println("All right!");continue;}


				Question q2 = getQuestion(input);
				getAnswer(q2.getAnswer());

			}




		}

	}
}
