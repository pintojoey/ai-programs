
public class Answer {
	
		private String answer_statement;
		private int counter_question;
		private int id;
		/**
		 * @return the answer_statement
		 */
		public String getAnswer_statement() {
			return answer_statement;
		}
		/**
		 * @param answer_statement the answer_statement to set
		 */
		public void setAnswer_statement(String answer_statement) {
			this.answer_statement = answer_statement;
		}
		/**
		 * @return the counter_question
		 */
		public int getCounter_question() {
			return counter_question;
		}
		/**
		 * @param counter_question the counter_question to set
		 */
		public void setCounter_question(int counter_question) {
			this.counter_question = counter_question;
		}
		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
		/**
		 * @param id the id to set
		 */
		public void setId(int id) {
			this.id = id;
		}
		
	
}
