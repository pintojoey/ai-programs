import java.util.Scanner;

public class Coffee_Machine {
	static int heat=60;
	static int tea=10;
	static int milk=100;
	static int coffee=100;
	static int water=200;
	static int lemon_tea=100;
	static String supply="220V/AC";

	public static void refill(){
		heat=60;
		tea=100;
		milk=100;
		coffee=100;
		water=200;
		lemon_tea=100;
	}
	public static boolean make(int used_tea,int used_coffee,int used_milk,int used_water,int used_lemon_tea){
		if(used_tea>tea){System.out.println("Out of tea");return false;}
		else if(used_coffee>coffee){System.out.println("Out of coffee");return false;}
		else if(used_milk>milk){System.out.println("Out of milk");return false;}
		else if(used_water>water){System.out.println("Out of water");return false;}
		else if(used_lemon_tea>lemon_tea){System.out.println("Out of lemon");return false;}
		else{
			tea-=used_tea;
			coffee-=used_coffee;
			milk-=used_milk;
			water-=used_water;
			lemon_tea-=used_lemon_tea;
			return true;
		}	



	}
	public static void check(){
		if(10>tea){System.out.println("Low on tea");}
		else if(10>coffee){System.out.println("Low on coffee");}
		else if(10>milk){System.out.println("Low on milk");}
		else if(20>water){System.out.println("Low on water");}
		else if(10>lemon_tea){System.out.println("Low on lemon tea");}
	}

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		Scanner in=new Scanner(System.in);
		System.out.println("Welcome to Coffee Maker!");
		//Press 1 for Tea
		//Press 2 for Coffee
		//Press 3 for Milk
		//Press 4 for LemonTea
		//Press 5 for Hot Water
		//Press 6 for Setup
		//Press 1 for Voltage Supply Management
		//Press 2 for 
		Thread.sleep(1000);




		boolean exit=false;
		while(true){
			System.out.println("Waiting For Input");
			int input=in.nextInt();
			switch(input){
			case 1:

				if(!make(5,0,5,5,0)){break;}
				System.out.println("Dispensing Tea..");
				Thread.sleep(4000);
				System.out.println("Tea Dispensed!");
				break;
			case 2:

				if(!make(0,5,5,5,0)){break;}
				System.out.println("Dispensing Coffee..");
				Thread.sleep(4000);
				System.out.println("Coffee Dispensed!");
				break;
			case 3:
				if(!make(0,0,10,0,0)){break;}
				System.out.println("Dispensing Milk..");
				Thread.sleep(4000);
				System.out.println("Milk Dispensed!");
				break;
			case 4:
				if(!make(2,0,0,10,5)){break;}
				System.out.println("Dispensing Lemon Tea..");
				Thread.sleep(4000);
				System.out.println("Lemon Tea Dispensed!");
				break;
			case 5:
				if(!make(0,0,0,10,0)){break;}
				System.out.println("Dispensing Hot Water.");
				Thread.sleep(4000);
				System.out.println("Hot Water Dispensed!");
				break;
			case 6:
				System.out.println("Entering Setup Mode");
				Thread.sleep(1000);
				System.out.println("Choose an option");
				input=in.nextInt();
				switch(input){
				case 1:
					System.out.println("System Supply");
					input=in.nextInt();
					switch(input){
					case 1:
						System.out.println(supply);
						break;
					case 2:
						supply="220V/AC";
						System.out.println("Supply is now "+supply);
						break;
					case 3:
						supply="110V/AC";
						System.out.println("Supply is now "+supply);
						break;
					}
					break;
				case 2:
					System.out.println("System Temperature");
					input=in.nextInt();
					switch(input){
					case 1:
						System.out.println(heat);
						break;
					case 2:
						System.out.println("Temperature adjustment");
						while(!exit){
							input=in.nextInt();
							switch(input){
							case 1:
								if(heat<50){
									heat+=5;
									System.out.println(heat+"C");
								}
								else
									System.out.println("Temp Maximum");
								break;
							case 2:
								if(heat>20){
									heat-=5;
									System.out.println(heat+"C");
								}
								else
									System.out.println("Temp Minimum");
								break;
							case 3: 
								exit=false;
								break;

							}



							heat-=5;
							System.out.println("Heat is now "+heat);
						}
						break;

					}
					break;
				case 3:
					System.out.println("Cleaning Mode On!");
					Thread.sleep(1000);
					System.out.println("Draining...");
					Thread.sleep(1000);
					System.out.println("Cleaner Refill...");
					Thread.sleep(1000);
					System.out.println("Rinsing...");
					Thread.sleep(1000);
					System.out.println("Drying...");
					Thread.sleep(1000);
					System.out.println("Refilling...");
					refill();
					Thread.sleep(4000);
					System.out.println("Ready...");
					break;
				case 4:
					System.out.println("Checking containers");
					Thread.sleep(1000);
					check();
					break;

				}


				break;
			case 7:
				System.out.println("Shutting Down..");
				Thread.sleep(3000);
				System.out.println("Bye Bye!");
				System.exit(0);
				break;
			default:

				break;

			}
			Thread.sleep(2000);
		}

	}

}
